package nl.snowmanxl.endpoint;

import nl.snowmanxl.aangifte.AangifteBean;
import nl.snowmanxl.aangifte.BerekendeAangifte;
import nl.snowmanxl.aangifte.GetAangifteDetailsRequest;
import nl.snowmanxl.aangifte.GetAangifteDetailsResponse;
import nl.snowmanxl.berekening.BelastingBerekening;
import nl.snowmanxl.service.AangifteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class AangifteDetailsEndpoint {

    @Autowired
    private BelastingBerekening berekening;

    @Autowired
    private AangifteService service;

    @PayloadRoot(namespace = "http://snowmanxl.nl/aangifte", localPart = "GetAangifteDetailsRequest")
    @ResponsePayload
    public GetAangifteDetailsResponse processAangifteDetailsRequest(@RequestPayload GetAangifteDetailsRequest request) {
        GetAangifteDetailsResponse response = new GetAangifteDetailsResponse();

        BerekendeAangifte berekendeAangifte = new BerekendeAangifte();
        berekendeAangifte.setId((int) System.currentTimeMillis());
        berekendeAangifte.setInkomenNaBelasting(berekening.berekenNettoInkomenPerJaar(request.getInkomen()));
        response.setBerekendeaangifte(berekendeAangifte);

        AangifteBean bean = new AangifteBean();
        bean.setAangifte(berekendeAangifte.getInkomenNaBelasting());

        service.saveAangifte(bean);

        return response;
    }

}
