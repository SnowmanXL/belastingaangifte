package nl.snowmanxl.berekening;

import org.springframework.stereotype.Component;

@Component
public class BelastingBerekening {

    private int Schaal1 = 2000;
    private int Schaal2 = 3500;
    private int Schaal3 = 4000;
    private int Schaal4 = 15000;

    private double brutoInkomen;


    public double berekenNettoInkomenPerJaar(Double brutoInkomenPerMaand){
        brutoInkomen = brutoInkomenPerMaand;

        if( brutoInkomen - Schaal1 < 0 ) {
             return( brutoInkomen - berekenBelastingPerMaand(brutoInkomenPerMaand,22))*12;
        }

        if( brutoInkomen - Schaal2 < 0 ) {
            double berekendebelasting = berekenBelastingPerMaand(2000,22);
            berekendebelasting += berekenBelastingPerMaand(brutoInkomen - 2000, 30);
            return (brutoInkomen - berekendebelasting)*12;
        }

        if( brutoInkomen - Schaal3 < 0 ) {
            double berekendebelasting = berekenBelastingPerMaand(2000,22);
            berekendebelasting += berekenBelastingPerMaand(1500, 30);
            berekendebelasting += berekenBelastingPerMaand(brutoInkomen - 3500, 44);
            return (brutoInkomen - berekendebelasting)*12;
        }

        else {
            double berekendebelasting = berekenBelastingPerMaand(2000,22);
            berekendebelasting += berekenBelastingPerMaand(1500, 30);
            berekendebelasting += berekenBelastingPerMaand(500, 44);
            berekendebelasting += berekenBelastingPerMaand(brutoInkomen - 4000, 55);
            return (brutoInkomen - berekendebelasting)*12;
        }
    }

    private double berekenBelastingPerMaand(double bedragPerMaand, double percentage){
        return brutoInkomen * (percentage / 100);
    }



}
