package nl.snowmanxl.service;

import nl.snowmanxl.aangifte.AangifteBean;
import nl.snowmanxl.aangifte.BerekendeAangifte;
import nl.snowmanxl.repository.AangifteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AangifteService {

    @Autowired
    private AangifteRepository repository;

    public void saveAangifte(AangifteBean aangifte) {
        repository.save(aangifte);
    }

    public List<AangifteBean> getAllAangiftes() {
        return repository.findAll();
    }
}
