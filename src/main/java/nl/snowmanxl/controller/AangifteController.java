package nl.snowmanxl.controller;

import nl.snowmanxl.aangifte.AangifteBean;
import nl.snowmanxl.aangifte.BerekendeAangifte;
import nl.snowmanxl.service.AangifteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/aangiftes")
public class AangifteController {

    @Autowired
    private AangifteService service;

    @GetMapping
    public List<AangifteBean> getAllCursussen() {
        return service.getAllAangiftes();
    }
}
