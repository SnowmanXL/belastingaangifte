package nl.snowmanxl.aangifte;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "AANGIFTE")
public class AangifteBean implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NETTO_INKOMEN")
    private double nettoInkomen;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getAangifte() {
        return nettoInkomen;
    }

    public void setAangifte(double aangifte) {
        this.nettoInkomen = aangifte;
    }
}
