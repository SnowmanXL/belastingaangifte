package nl.snowmanxl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelastingApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelastingApplication.class, args);
	}
}
