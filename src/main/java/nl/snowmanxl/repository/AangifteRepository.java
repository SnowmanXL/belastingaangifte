package nl.snowmanxl.repository;


import nl.snowmanxl.aangifte.AangifteBean;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AangifteRepository extends JpaRepository<AangifteBean,Long> {

}
